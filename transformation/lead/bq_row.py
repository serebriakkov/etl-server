import os

from load import lead_pb2
from transformation.lead.transform_data import *
from load.load import BigQueryData


def construct_bq_rows(
        leads: list[dict],
        start_time: datetime = None,
        end_time: datetime = None) -> BigQueryData:

    inserted_rows = []
    updated_rows = []

    for lead in leads:
        if start_time and end_time:
            if start_time < lead['DATE_CREATE'] < end_time:
                inserted_rows.append(_create_serialized_row(_create_row(lead)))
            else:
                updated_rows.append(_create_row(lead))
        else:
            inserted_rows.append(_create_serialized_row(_create_row(lead)))

    bigquery_data = BigQueryData(
        project_id=os.getenv('PROJECT_ID'),
        dataset_id=os.getenv('DATASET_ID'),
        table_id=os.getenv('LEADS_TABLE_ID'),
        copy_to_proto=lead_pb2.Lead.DESCRIPTOR.CopyToProto,
        inserted_rows=inserted_rows if len(inserted_rows) > 0 else None,
        updated_rows=updated_rows if len(updated_rows) > 0 else None
    )

    return bigquery_data


def _create_serialized_row(lead: {}) -> str:
    row = lead_pb2.Lead()
    for key in lead.keys():
        if lead[key] is not None:
            setattr(row, key, lead[key])
    return row.SerializeToString()


def _create_row(lead: {}) -> {}:
    return {
        'ID': lead['ID'],
        'CONTACT_ID': lead['CONTACT_ID'],
        'DATE_CREATE': lead['DATE_CREATE'].isoformat(),
        'DATE_MODIFY': lead['DATE_MODIFY'].isoformat(),
        'STATUS': extract_status(lead['STATUS_ID']),
        'EMAIL': get_email(lead['ID']),
        'APP_FORM': get_app_form(lead['ID']),
        'COMMUNICATION_METHOD': extract_communication_method(lead['ID']),
        'SOURCE': extract_source(lead['SOURCE_ID']),
        'REASON_NOT_IMPLEMENTING': extract_reason_not_implementing(lead['ID']),
        'UTM_SOURCE': None,
        'UTM_MEDIUM': None,
        'UTM_TERM': None,
        'UTM_CONTENT': None,
        'UTM_CAMPAIGN': None,
        **extract_utm(lead['ID']),
        'TYPE_OF_APPEAL': get_type_of_appeal(lead['ID']),
        'SUPPORT_LINE': extract_support_line(lead['ID']),
        'RESPONSIBLE': get_responsible(lead['ASSIGNED_BY_ID'])
    }

