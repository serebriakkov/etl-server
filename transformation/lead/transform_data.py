from db.lead_data import *


def get_email(lead_id: int) -> str | None:
    emails = [email['VALUE'] for email in extract_email(lead_id)]
    if len(emails) > 0:
        return ','.join(emails)
    else:
        return None


def get_app_form(lead_id: int) -> str | None:
    result = extract_app_form(lead_id)
    if result and len(result) > 0:
        return ','.join(extract_app_form(lead_id))
    else:
        return None


def get_type_of_appeal(lead_id: int) -> str | None:
    type_of_appeal_list = extract_type_of_appeal(lead_id)
    if type_of_appeal_list and len(type_of_appeal_list) > 0:
        return ','.join(type_of_appeal_list)
    else:
        return None


def get_responsible(responsible_id: int) -> str | None:
    responsible = extract_responsible(responsible_id)
    if responsible:
        return f"{responsible['NAME']} {responsible['LAST_NAME']}"
    else:
        return None
