import os

from transformation.deal.transform_data import *
from load import deal_pb2
from load.load import BigQueryData


def construct_bq_rows(deals: list[dict],
                      start_time: datetime = None,
                      end_time: datetime = None) -> BigQueryData:

    inserted_rows = []
    updated_rows = []

    for deal in deals:
        if start_time and end_time:
            if start_time < deal['DATE_CREATE'] < end_time:
                inserted_rows.append(_create_serialized_row(_create_row(deal)))
            else:
                updated_rows.append(_create_row(deal))
        else:
            inserted_rows.append(_create_serialized_row(_create_row(deal)))

    bigquery_data = BigQueryData(
        project_id=os.getenv('PROJECT_ID'),
        dataset_id=os.getenv('DATASET_ID'),
        table_id=os.getenv('DEALS_TABLE_ID'),
        copy_to_proto=deal_pb2.Deal.DESCRIPTOR.CopyToProto,
        inserted_rows=inserted_rows if len(inserted_rows) > 0 else None,
        updated_rows=updated_rows if len(updated_rows) > 0 else None
    )

    return bigquery_data


def _create_row(deal: {}) -> {}:
    return {
        'ID': deal['ID'],
        'DATE_CREATE': deal['DATE_CREATE'].isoformat(),
        'DATE_MODIFY': deal['DATE_MODIFY'].isoformat(),
        'TYPE': extract_deal_type(deal['TYPE_ID']),
        'AMOUNT': deal['OPPORTUNITY'],
        'PAYMENT_STATE': extract_deal_payment_state(deal['ID']),
        'STAGE': extract_deal_stage(deal['STAGE_ID']),
        'ORDER_ID': extract_order_id(deal['ID']),
        'ORDER_TYPE': extract_order_type(deal['ID']),
        'CONTACT_ID': deal['CONTACT_ID'],
        'UTM_SOURCE': None,
        'UTM_MEDIUM': None,
        'UTM_CAMPAIGN': None,
        'UTM_TERM': None,
        'UTM_CONTENT': None,
        **extract_utm(deal['ID']),
        'TRANSACTIONS_NUMBER': extract_transactions_number(deal['ID']),
        'ORDER_LINK': extract_order_link(deal['ID']),
        'DECLARATION_PREPARE_STATUS': extract_declaration_prepare_status(deal['ID']),
        'DECLARATION_PREPARE_DATE': get_declaration_prepare_date(deal['ID']),
        'RPZ_CHECK_STATUS': extract_rpz_check_status(deal['ID']),
        'RPZ_CHECK_DATE': get_rpz_check_date(deal['ID']),
        'PROMO_CODE': extract_promo_code(deal['ID']),
        'GOOGLE_DRIVE_FOLDER': extract_google_drive_folder(deal['ID']),
        'GOOGLE_FOLDER_NUMBER': extract_google_drive_folder_number(deal['ID']),
        'BROKER': get_broker(deal['ID']),
        'MISSING_POSITIONS': get_missing_position(deal['ID']),
        'REJECTION_REASON': extract_rejection_reason(deal['ID']),
        'STOP_REASON': extract_stop_reason(deal['ID']),
        'PRIORITY': extract_priority(deal['ID']),
        'ORDER_DATE': get_order_date(deal['ID']),
        'REPORTING_YEAR': extract_reporting_year(deal['ID']),
        'OFFICE_CHECK': extract_office_check(deal['ID']),
        'PRESENCE_OF_CUSTOM_BROKER': get_presence_of_custom_broker(deal['ID']),
        'NAME': extract_name(deal['ID']),
        'LAST_NAME': extract_last_name(deal['ID']),
        'SECOND_NAME': extract_second_name(deal['ID']),
        'PHONE_NUMBER': extract_phone_number(deal['ID']),
        'GURU_FILES_STATUS': extract_guru_files_status(deal['ID']),
        'DIVIDEND_STATUS': extract_dividend_status(deal['ID']),
        'ADDITIONAL_STATUS_INCOME': extract_additional_income_status(deal['ID']),
        'SOURCE': extract_source(deal['SOURCE_ID']),
        'DEAL_STAGE_HISTORY': get_deal_stage_history(deal['ID']),
        'TAX_PAYABLE_13': get_tax_payable_13(deal['ID']),
        'TAX_PAYABLE_15': get_tax_payable_15(deal['ID']),
        'ACTUAL_ORDER': extract_actual_order(deal['ID']),
        'PAYMENT_DATE': get_payment_date(deal['ID'])
    }


def _create_serialized_row(deal: {}) -> str:
    row = deal_pb2.Deal()
    for key in deal.keys():
        if deal[key] is not None:
            if key == 'ORDER_ID':
                setattr(row, key, int(deal[key]))
            else:
                setattr(row, key, deal[key])
    return row.SerializeToString()
