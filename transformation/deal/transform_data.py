from db.deal_data import *


def get_broker(deal_id: int) -> str:
    """Метод формирует строку со списком брокеров из сделки,
    в формате обработка которого настроена в PowerBI."""
    all_broker_list = extract_all_broker_list()
    deal_broker_list = extract_deal_broker_list(deal_id)
    result = []
    # TODO Возможно стоит реализовать обработку пустого
    #  списка deal_broker_list
    if deal_broker_list:
        for i in range(len(all_broker_list)):
            if all_broker_list[i] not in deal_broker_list:
                result.append(' ')
            else:
                result.append(all_broker_list[i])
        return ','.join(result)
    else:
        result = [' ' for broker in all_broker_list]
        return ','.join(result)


def get_deal_stage_history(deal_id: int) -> str:
    deal_stage_history = extract_deal_stage_history(deal_id)
    result = []

    for stage in deal_stage_history:
        stage_name = extract_deal_stage(stage["STAGE_ID"])
        if stage_name:
            result.append(
                f'"{stage_name}":"{stage["CREATED_TIME"]}"'
            )

    return ','.join(result)


def get_missing_position(deal_id: int) -> bool | None:
    missing_position = extract_missing_position(deal_id)
    if missing_position:
        return True if missing_position == 1 else False
    else:
        return None


def get_presence_of_custom_broker(deal_id: int) -> bool | None:
    presence_of_custom_broker = extract_presence_of_custom_broker(deal_id)
    if presence_of_custom_broker:
        return True if presence_of_custom_broker == 1 else False
    else:
        return None


def get_tax_payable_13(deal_id: int) -> str | None:
    tax_payable_13 = extract_tax_payable_13(deal_id)
    if tax_payable_13:
        return tax_payable_13.partition('|')[0]
    else:
        return None


def get_tax_payable_15(deal_id: int) -> str | None:
    tax_payable_15 = extract_tax_payable_15(deal_id)
    if tax_payable_15:
        return tax_payable_15.partition('|')[0]
    else:
        return None


def get_declaration_prepare_date(deal_id: int) -> str | None:
    declaration_prepare_date = extract_declaration_prepare_date(deal_id)
    if declaration_prepare_date:
        return declaration_prepare_date.isoformat()
    else:
        return None


def get_rpz_check_date(deal_id: int) -> str | None:
    rpz_check_date = extract_rpz_check_date(deal_id)
    if rpz_check_date:
        return rpz_check_date.isoformat()
    else:
        return None


def get_order_date(deal_id: int) -> str | None:
    order_date = extract_order_date(deal_id)
    if order_date:
        return order_date.isoformat()
    else:
        return None


def get_payment_date(deal_id: int) -> str | None:
    payment_date = extract_payment_date(deal_id)
    if payment_date:
        return payment_date.isoformat()
    else:
        return None