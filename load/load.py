from typing import NamedTuple

from google.cloud import bigquery_storage_v1
from google.cloud.bigquery import Table
from google.cloud.bigquery_storage_v1 import types
from google.cloud.bigquery_storage_v1 import writer
from google.cloud import bigquery
from google.protobuf import descriptor_pb2


class BigQueryData(NamedTuple):
    project_id: str
    dataset_id: str
    table_id: str
    copy_to_proto: callable
    inserted_rows: list[str] | None
    updated_rows: list[dict] | None


def load_bq_data(bigquery_data: BigQueryData) -> None:
    if bigquery_data.inserted_rows:
        append_rows_pending(bigquery_data)
    if bigquery_data.updated_rows:
        update_row(bigquery_data)


def append_rows_pending(bigquery_data: BigQueryData) -> None:
    """Create a write stream, write some sample data, and commit the stream."""
    write_client = bigquery_storage_v1.BigQueryWriteClient()
    parent = write_client.table_path(
        bigquery_data.project_id,
        bigquery_data.dataset_id,
        bigquery_data.table_id
    )
    write_stream = types.WriteStream()

    # When creating the stream, choose the type. Use the PENDING type to wait
    # until the stream is committed before it is visible. See:
    # https://cloud.google.com/bigquery/docs/reference/storage/rpc/google.cloud.bigquery.storage.v1#google.cloud.bigquery.storage.v1.WriteStream.Type
    write_stream.type_ = types.WriteStream.Type.PENDING
    write_stream = write_client.create_write_stream(
        parent=parent, write_stream=write_stream
    )
    stream_name = write_stream.name

    # Create a template with fields needed for the first request.
    request_template = types.AppendRowsRequest()

    # The initial request must contain the stream name.
    request_template.write_stream = stream_name

    # So that BigQuery knows how to parse the serialized_rows, generate a
    # protocol buffer representation of your message descriptor.
    proto_schema = types.ProtoSchema()
    proto_descriptor = descriptor_pb2.DescriptorProto()
    bigquery_data.copy_to_proto(proto_descriptor)
    proto_schema.proto_descriptor = proto_descriptor
    proto_data = types.AppendRowsRequest.ProtoData()
    proto_data.writer_schema = proto_schema
    request_template.proto_rows = proto_data

    # Some stream types support an unbounded number of requests. Construct an
    # AppendRowsStream to send an arbitrary number of requests to a stream.
    append_rows_stream = writer.AppendRowsStream(write_client, request_template)

    # Create a batch of row data by appending proto2 serialized bytes to the
    # serialized_rows repeated field.
    proto_rows = types.ProtoRows()
    for row in bigquery_data.inserted_rows:
        proto_rows.serialized_rows.append(row)

    # Set an offset to allow resuming this stream if the connection breaks.
    # Keep track of which requests the server has acknowledged and resume the
    # stream at the first non-acknowledged message. If the server has already
    # processed a message with that offset, it will return an ALREADY_EXISTS
    # error, which can be safely ignored.
    #
    # The first request must always have an offset of 0.
    request = types.AppendRowsRequest()
    request.offset = 0
    proto_data = types.AppendRowsRequest.ProtoData()
    proto_data.rows = proto_rows
    request.proto_rows = proto_data

    response_future_1 = append_rows_stream.send(request)

    print(response_future_1.result())

    # Shutdown background threads and close the streaming connection.
    append_rows_stream.close()

    # A PENDING type stream must be "finalized" before being committed. No new
    # records can be written to the stream after this method has been called.
    write_client.finalize_write_stream(name=write_stream.name)

    # Commit the stream you created earlier.
    batch_commit_write_streams_request = types.BatchCommitWriteStreamsRequest()
    batch_commit_write_streams_request.parent = parent
    batch_commit_write_streams_request.write_streams = [write_stream.name]
    write_client.batch_commit_write_streams(batch_commit_write_streams_request)

    print(f"Writes to stream: '{write_stream.name}' have been committed.")


def update_row(bigquery_data: BigQueryData) -> None:
    client = bigquery.client.Client()
    table = client.get_table(
        f'{bigquery_data.project_id}.'
        f'{bigquery_data.dataset_id}.'
        f'{bigquery_data.table_id}'
    )

    sql = _create_query(table)

    for row in bigquery_data.updated_rows:
        query_job_config = bigquery.QueryJobConfig(
            query_parameters=_create_query_params(table, row))
        query_job = client.query(sql, query_job_config)
        print(query_job.result())


def _create_query(table: Table) -> str:
    field_set = []

    for column in table.schema:
        field_set.append(f'{column.name} = @{column.name}')

    return f"""
        UPDATE {table.project}.{table.dataset_id}.{table.table_id}
        SET {", ".join(field_set)}
        WHERE ID = @ID
    """


def _create_query_params\
                (table: Table, row: {}) -> list[bigquery.ScalarQueryParameter]:
    result = []

    for column in table.schema:
        if column.field_type == 'BOOLEAN':
            parameter = bigquery.ScalarQueryParameter(column.name,
                                                      'BOOL',
                                                      row.get(column.name))
            result.append(parameter)
        else:
            parameter = bigquery.ScalarQueryParameter(column.name,
                                                      column.field_type,
                                                      row.get(column.name))
            result.append(parameter)

    return result
