from mysql.connector import Error

from db.lead_data import extract_leads
from transformation.lead.bq_row import construct_bq_rows
from load.load import load_bq_data
from util.logger import getLogger


def main() -> None:
    logger = getLogger(__name__)
    logger.info('ETL process started')

    try:
        leads = extract_leads()
        bq_rows = construct_bq_rows(leads)
        load_bq_data(bq_rows)
    except Error as e:
        logger.warning('Failed to get leads', exc_info=e)

    logger.info('ETL process completed')


if __name__ == '__main__':
    main()
