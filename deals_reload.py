from mysql.connector import Error

from db.deal_data import extract_deals
from transformation.deal.bq_row import construct_bq_rows
from load.load import load_bq_data
from util.logger import getLogger


def main() -> None:
    logger = getLogger(__name__)
    logger.info('ETL process started')

    try:
        deals = extract_deals()
        bq_rows = construct_bq_rows(deals)
        load_bq_data(bq_rows)
    except Error as e:
        logger.warning('Failed to get deals', exc_info=e)

    logger.info('ETL process completed')


if __name__ == '__main__':
    main()