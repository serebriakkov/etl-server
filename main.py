from datetime import datetime, timedelta

from mysql.connector import Error

from db.deal_data import extract_deals
from db.lead_data import extract_leads
from transformation.deal import bq_row as deal
from transformation.lead import bq_row as lead
from load.load import load_bq_data
from util.logger import getLogger


def main() -> None:
    logger = getLogger(__name__)
    logger.info('ETL process started')

    start_time = (datetime.now() - timedelta(hours=2))
    end_time = (datetime.now() - timedelta(hours=1))

    try:
        deals = extract_deals(start_time, end_time)
        if len(deals) > 0:
            bq_rows = deal.construct_bq_rows(deals, start_time, end_time)
            load_bq_data(bq_rows)
    except Error as e:
        logger.warning('Failed to get deals', exc_info=e)

    try:
        leads = extract_leads(start_time, end_time)
        if len(leads) > 0:
            bq_rows = lead.construct_bq_rows(leads, start_time, end_time)
            load_bq_data(bq_rows)
    except Error as e:
        logger.warning('Failed to get leads', exc_info=e)

    logger.info('ETL process completed')


if __name__ == '__main__':
    main()
