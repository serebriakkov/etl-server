import logging.config
from logging import getLogger

LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'myFormatter': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'fileHandler': {
            'level': 'INFO',
            'formatter': 'myFormatter',
            'class': 'logging.FileHandler',
            'filename': 'log/log.txt',
        },
        'debug_console_handler': {
            'level': 'DEBUG',
            'formatter': 'myFormatter',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
    },
    'loggers': {
        'db.lead_data': {
            'handlers': ['fileHandler', 'debug_console_handler'],
            'level': 'INFO',
            'propagate': False
        },
        'db.deal_data': {
            'handlers': ['fileHandler', 'debug_console_handler'],
            'level': 'INFO',
            'propagate': False
        },
        'db.connect': {
            'handlers': ['fileHandler', 'debug_console_handler'],
            'level': 'INFO',
            'propagate': False
        },
        '__main__': {
            'handlers': ['fileHandler', 'debug_console_handler'],
            'level': 'INFO',
            'propagate': False
        },
    }
}
logging.config.dictConfig(LOGGING_CONFIG)
