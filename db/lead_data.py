from datetime import datetime

from mysql.connector import Error
from phpserialize import unserialize

from db.connect import cursor
from util import logger

logger = logger.getLogger(__name__)


def extract_leads(
        start_time: datetime = None,
        end_time: datetime = None) -> list[dict]:

    if start_time and end_time:
        query = """
            SELECT *
            FROM b_crm_lead
            WHERE DATE_MODIFY BETWEEN %s AND %s
        """
        cursor.execute(query, (start_time, end_time))
        return cursor.fetchall()
    else:
        query = 'SELECT * FROM b_crm_lead'
        cursor.execute(query)
        return cursor.fetchall()


def extract_status(status_id: str) -> str:
    try:
        query = """
            SELECT NAME
            FROM b_crm_status
            WHERE ENTITY_ID = 'STATUS'
                AND STATUS_ID = %s
        """
        cursor.execute(query, (status_id,))
        result = cursor.fetchone()
        return result['NAME']
    except Error as e:
        logger.warning('Failed to get status.', exc_info=e)


def extract_email(lead_id: int) -> list[dict]:
    try:
        query = """
            SELECT VALUE
            FROM b_crm_field_multi
            WHERE ENTITY_ID = 'LEAD'
                AND TYPE_ID = 'EMAIL'
                AND ELEMENT_ID = %s
        """
        cursor.execute(query, (lead_id,))
        return cursor.fetchall()
    except Error as e:
        logger.warning('Failed to get email.', exc_info=e)


def extract_app_form(lead_id: int) -> list[str] | None:
    try:
        user_field_id = 'UF_CRM_1575884674'
        return _get_user_field_enum_values(lead_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get app form.', exc_info=e)


def extract_communication_method(lead_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1608192594'
        return _get_user_field_enum_value(lead_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get communication method.', exc_info=e)


def extract_source(source_id: str | None) -> str | None:
    if source_id:
        try:
            query = """
                SELECT NAME
                FROM b_crm_status
                WHERE STATUS_ID = %s
                    AND ENTITY_ID = 'SOURCE'
            """
            cursor.execute(query, (source_id,))
            result = cursor.fetchone()
            return result['NAME'] if result else None
        except Error as e:
            logger.warning('Failed to get source.', exc_info=e)
    else:
        return None


def extract_reason_not_implementing(lead_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1599301537766'
        return _get_user_field_enum_value(lead_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get reason not implementing.', exc_info=e)


def extract_utm(lead_id: int) -> dict[str, str | None]:
    try:
        query = """
            SELECT CODE, VALUE
            FROM b_crm_utm
            WHERE ENTITY_ID = %s
        """
        cursor.execute(query, (lead_id,))
        result = cursor.fetchall()
        utm_set = {}

        for utm in result:
            utm_set[utm['CODE']] = utm['VALUE']

        return utm_set
    except Error as e:
        logger.warning('Failed to get utm set.', exc_info=e)


def extract_type_of_appeal(lead_id: int) -> list[str] | None:
    try:
        user_field_id = 'UF_CRM_1622877434760'
        return _get_user_field_enum_values(lead_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get type of appeal.', exc_info=e)


def extract_support_line(lead_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1634818901'
        return _get_user_field_enum_value(lead_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get support line.', exc_info=e)


def extract_responsible(responsible_id: int) -> dict[str, str] | None:
    try:
        query = """
            SELECT NAME, LAST_NAME
            FROM b_user
            WHERE ID = %s
        """
        cursor.execute(query, (responsible_id,))
        result = cursor.fetchone()
        return result
    except Error as e:
        logger.warning('Failed to get responsible.', exc_info=e)


def _get_user_field_enum_value(lead_id: int, field_id: str) -> str | None:
    # Get value id
    query = f"""
        SELECT {field_id}
        FROM b_uts_crm_lead
        WHERE VALUE_ID = %s
    """
    cursor.execute(query, (lead_id,))
    value_id = cursor.fetchone()

    if value_id and value_id[field_id]:
        # Get value name
        query = """
            SELECT VALUE
            FROM b_user_field_enum
            WHERE ID = %s
        """
        cursor.execute(query, (value_id[field_id],))
        result = cursor.fetchone()
        return result['VALUE'] if result else None
    else:
        return None


def _get_user_field_enum_values(lead_id: int, field_id: str) -> list[str] | None:
    # Get value ids
    query = f"""
        SELECT {field_id}
        FROM b_uts_crm_lead
        WHERE VALUE_ID = %s
    """
    cursor.execute(query, (lead_id,))
    value_ids = cursor.fetchone()

    if value_ids and value_ids[field_id]:
        result = None
        value_ids = unserialize(value_ids[field_id].encode('utf-8')).values()

        if len(value_ids) > 0:
            # Get value names
            result = []
            value_ids = [str(id) for id in value_ids]
            query = f"""
                SELECT VALUE
                FROM b_user_field_enum
                WHERE ID IN ({','.join(value_ids)})
            """
            cursor.execute(query)

            for name in cursor.fetchall():
                result.append(name['VALUE'])

        return result
    else:
        return None
