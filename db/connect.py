import os

import mysql.connector
from mysql.connector import Error

from util.logger import getLogger

cursor = None
logger = getLogger(__name__)

try:
    connection = mysql.connector.connect(
        host=os.getenv('DB_HOST'),
        user=os.getenv('DB_USER'),
        passwd=os.getenv('DB_PASSWORD'),
        database=os.getenv('DB_NAME')
    )
    cursor = connection.cursor(dictionary=True, buffered=True)
    logger.info('Connection to MySQL DB successful')
except Error as e:
    logger.error('Failed to connect to DB.', exc_info=e)
    exit()
