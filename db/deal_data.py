from datetime import datetime
from typing import Any

from mysql.connector import Error
from phpserialize import unserialize

from db.connect import cursor
from util import logger

logger = logger.getLogger(__name__)


def extract_deals(
        start_time: datetime = None,
        end_time: datetime = None) -> list[dict]:

    if start_time and end_time:
        query = """
            SELECT *
            FROM b_crm_deal
            WHERE DATE_MODIFY BETWEEN %s AND %s
        """
        cursor.execute(query, (start_time, end_time))
        return cursor.fetchall()
    else:
        query = 'SELECT * FROM b_crm_deal'
        cursor.execute(query)
        return cursor.fetchall()


def extract_deal_type(type_id: str | None) -> str | None:
    if type_id and type_id != '':
        try:
            query = """
                SELECT NAME
                FROM b_crm_status
                WHERE STATUS_ID = %s
                    AND ENTITY_ID = 'DEAL_TYPE'
            """
            cursor.execute(query, (type_id,))
            result = cursor.fetchone()
            return result['NAME']
        except Error as e:
            logger.warning('Failed to get deal type.', exc_info=e)
    else:
        return None


def extract_deal_payment_state(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1579504901544'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get deal payment state.', exc_info=e)


def extract_deal_stage(stage_id: str) -> str | None:
    try:
        query = """
            SELECT NAME
            FROM b_crm_status
            WHERE ENTITY_ID LIKE('DEAL_STAGE%')
                AND STATUS_ID = %s
        """
        cursor.execute(query, (stage_id,))
        result = cursor.fetchone()
        return result['NAME'] if result else None
    except Error as e:
        logger.warning('Failed to get deal stage.', exc_info=e)


def extract_order_id(deal_id: int) -> int | None:
    try:
        user_field_id = 'UF_CRM_1576067402'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get order id.', exc_info=e)


def extract_order_type(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1600354440'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get order type.', exc_info=e)


def extract_utm(deal_id: int) -> dict[str, str | None]:
    try:
        query = """
            SELECT CODE, VALUE
            FROM b_crm_utm
            WHERE ENTITY_ID = %s
        """
        cursor.execute(query, (deal_id,))
        result = cursor.fetchall()
        utm_set = {}

        for utm in result:
            utm_set[utm['CODE']] = utm['VALUE']

        return utm_set
    except Error as e:
        logger.warning('Failed to get utm set.', exc_info=e)


def extract_transactions_number(deal_id: int) -> int | None:
    try:
        user_field_id = 'UF_TRANSACTIONS_NUM'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get transactions number.', exc_info=e)


def extract_order_link(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_ORDER_LINK'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get order link.', exc_info=e)


def extract_declaration_prepare_status(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1600415202'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get declaration prepare status.', exc_info=e)


def extract_declaration_prepare_date(deal_id: int) -> datetime | None:
    try:
        user_field_id = 'UF_CRM_1611044879605'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get declaration prepare date.', exc_info=e)


def extract_rpz_check_status(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1585572115565'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get rpz check status.', exc_info=e)


def extract_rpz_check_date(deal_id: int) -> datetime | None:
    try:
        user_field_id = 'UF_CRM_1611044743633'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get rpz check date.', exc_info=e)


def extract_promo_code(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1585922321713'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get promo code.', exc_info=e)


def extract_google_drive_folder(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1586506118'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get google drive folder.', exc_info=e)


def extract_google_drive_folder_number(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1586506237'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get google drive folder number.', exc_info=e)


def extract_all_broker_list() -> list[str]:
    try:
        query = """
            SELECT VALUE
            FROM b_user_field_enum
            WHERE USER_FIELD_ID = 489
            ORDER BY ID
        """
        cursor.execute(query)
        result = []
        for broker in cursor.fetchall():
            result.append(broker['VALUE'])
        return result
    except Error as e:
        logger.warning('Failed to get all broker list.', exc_info=e)


def extract_deal_broker_list(deal_id: int) -> list[str] | None:
    try:
        user_field_id = 'UF_CRM_1605179333'
        return _get_user_field_enum_values(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get deal broker list.', exc_info=e)


def extract_missing_position(deal_id: int) -> int | None:
    try:
        user_field_id = 'UF_CRM_1605181388'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get missing position.', exc_info=e)


def extract_rejection_reason(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1595333413383'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get rejection reason.', exc_info=e)


def extract_stop_reason(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1599735499133'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get stop reason.', exc_info=e)


def extract_priority(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1601997168145'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get priority.', exc_info=e)


def extract_order_date(deal_id: int) -> datetime | None:
    try:
        user_field_id = 'UF_CRM_1618228302786'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get order date.', exc_info=e)


def extract_reporting_year(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1624020617'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get reporting year.', exc_info=e)


def extract_office_check(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1624882825'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get office check.', exc_info=e)


def extract_actual_order(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1625130143'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get actual order.', exc_info=e)


def extract_presence_of_custom_broker(deal_id: int) -> int | None:
    try:
        user_field_id = 'UF_CRM_1637835483952'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get presence of custom broker.', exc_info=e)


def extract_name(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1611838454489'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get name.', exc_info=e)


def extract_last_name(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1611838442295'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get last name.', exc_info=e)


def extract_second_name(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1621943613'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get second name.', exc_info=e)


def extract_phone_number(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1611838464824'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get phone number.', exc_info=e)


def extract_guru_files_status(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1638794104803'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get guru files status.', exc_info=e)


def extract_dividend_status(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1639410707662'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get dividend status.', exc_info=e)


def extract_additional_income_status(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1638268044405'
        return _get_user_field_enum_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get additional income status.', exc_info=e)


def extract_source(source_id: str) -> str | None:
    if source_id:
        try:
            query = """
                SELECT NAME
                FROM b_crm_status
                WHERE STATUS_ID = %s
                    AND ENTITY_ID = 'SOURCE'
            """
            cursor.execute(query, (source_id,))
            result = cursor.fetchone()
            return result['NAME']
        except Error as e:
            logger.warning('Failed to get source.', exc_info=e)
    else:
        return None


def extract_deal_stage_history(deal_id: int) -> list[dict]:
    try:
        query = """
            SELECT CREATED_TIME, STAGE_ID
            FROM b_crm_deal_stage_history
            WHERE OWNER_ID = %s
        """
        cursor.execute(query, (deal_id,))
        result = cursor.fetchall()
        return result
    except Error as e:
        logger.warning('Failed to get deal stage history.', exc_info=e)


def extract_tax_payable_13(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1645430155254'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get tax payable 13.', exc_info=e)


def extract_tax_payable_15(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1645430182191'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get tax payable 15.', exc_info=e)


def extract_payment_date(deal_id: int) -> datetime:
    try:
        user_field_id = 'UF_CRM_1611222069'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get payment date.', exc_info=e)


def extract_referral_link(deal_id: int) -> str | None:
    try:
        user_field_id = 'UF_CRM_1611222069'
        return _get_user_field_value(deal_id, user_field_id)
    except Error as e:
        logger.warning('Failed to get referral link.', exc_info=e)


def _get_user_field_enum_value(deal_id: int, field_id: str) -> str | None:
    # Get value id
    query = f"""
        SELECT {field_id}
        FROM b_uts_crm_deal
        WHERE VALUE_ID = %s
    """
    cursor.execute(query, (deal_id,))
    value_id = cursor.fetchone()

    if value_id and value_id[field_id]:
        # Get value name
        query = """
            SELECT VALUE
            FROM b_user_field_enum
            WHERE ID = %s
        """
        cursor.execute(query, (value_id[field_id],))
        result = cursor.fetchone()
        return result['VALUE'] if result else None
    else:
        return None


def _get_user_field_enum_values(deal_id: int, field_id: str) -> list[str] | None:
    # Get value ids
    query = f"""
        SELECT {field_id}
        FROM b_uts_crm_deal
        WHERE VALUE_ID = %s
    """
    cursor.execute(query, (deal_id,))
    value_ids = cursor.fetchone()

    if value_ids and value_ids[field_id]:
        result = None
        value_ids = unserialize(value_ids[field_id].encode('utf-8')).values()

        if len(value_ids) > 0:
            # Get value names
            result = []
            value_ids = [str(id) for id in value_ids]
            query = f"""
                SELECT VALUE
                FROM b_user_field_enum
                WHERE ID IN ({','.join(value_ids)})
            """
            cursor.execute(query)

            for name in cursor.fetchall():
                result.append(name['VALUE'])

        return result
    else:
        return None


def _get_user_field_value(deal_id: int, field_id: str) -> Any:
    query = f"""
        SELECT {field_id}
        FROM b_uts_crm_deal
        WHERE VALUE_ID = %s
    """
    cursor.execute(query, (deal_id,))
    result = cursor.fetchone()
    if result:
        return result[field_id]
    else:
        return None